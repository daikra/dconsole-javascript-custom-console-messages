// JavaScript Custom Console Messages 1.6.7 by daikra. Updated 2022-01-08



// Examples
// dConsole("I have an apple");
// dConsole("Shreek is an ogre", "crimson", "#fff")
// dConsole("Lemons are yellow", "gradient-red-blue", "#000")
// dConsole("Color spectrum", "gradientRainbow", "#fff")




/* Check Browsers START */
	
	// Opera
	var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
	// Firefox
	var isFirefox = typeof InstallTrigger !== 'undefined';
	// Chrome
	var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);

/* Check Browsers END   */




/* dStroke START */
// Custom way to make uniform text stroke/outline
function dStroke (receivedSize, receivedColor, receivedStrokeRounds) {

	var stroke = "0 0 ";

	for (x=1;x<receivedStrokeRounds;x++) { stroke += receivedSize + " " + receivedColor + ", 0 0 "; }

	stroke = stroke.slice(0, -6);

	return stroke;

}

// Example: dStroke ( "1px", "#fff", "3" );

/* dStroke END   */




/* ☮ dConsole START ☮ */

	function dConsole ( message, background, color ) {

		if ( isChrome || isFirefox || isOpera ) {
			var css;
			var rainbow;

			// Defaults
			if ( background == null || background === "background" )	{ background = "#555"; }
			if ( color == null		|| color === "color")				{ color = "#FFF"; }



			/* Gradient Background START */

				// If background contains the word "gradient"
				if ( background.indexOf("gradient") > -1)	{
					// Divide the gradient String
					background = background.split("-");
					background = "linear-gradient(to bottom right, " + background[1] + ", " + background[2] + ")";
				}

				// Rainbow Background
				if ( background === "rainbow" )		{ background = "linear-gradient(320deg, #cb0000, #ff9000, #e3d000, #02ff07, #02beff, #0222ff, #ff02ac, #a610c2)"; }

			/* Gradient Background END   */

			css =
				"color: " + color + ";" + 
				"border-radius: " + "10px" + ";" + 
				"border: " + "1.2px solid #000" + ";" + 
				"padding: " + "1.2px 8px" + ";" + 
				"text-shadow: " + dStroke("1px", "#000", 10) + ";" + 
				"background: " + background + ";";

			console.log( "%c" + message, css);
		}

		else { console.log(message); }

	}

/* ☮ dConsole END ☮   */
